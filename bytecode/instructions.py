import types

class ExecutionTracker(object):
	pass

class InvalidInstructionError(Exception):
	def __init__(self, inst, message):
		super(InvalidInstructionError, self).__init__(message)
		self.Instruction = inst

class Instruction(object):
	types = []
	name_map = {}

	def __init__(self, offset):
		self.offset = offset

	def create_error(self, message):
		return InvalidInstructionError(self, message)

	@staticmethod
	def _add_to_array(a, pos, val, valDefault):
		if (pos < len(a)):
			a[pos] = val
			return
		for i in range(len(a), pos):
			a.append(valDefault) # Inflate with default value
		a.append(val)

	@classmethod
	def add_type(cls, code, name, addl=0, **kwargs):
		if (isinstance(addl, (type, types.ClassType))):
			clsNew = addl
		else:
			clsNew = type(('Instruction' + name), (Instruction,), {})
			clsNew._handle_addl(addl)
		clsNew._handle_args(**kwargs)
		clsNew.name = name
		clsNew.code = code
		cls.register(clsNew)
		Instruction._add_to_array(cls.types, code, clsNew, None)
	@classmethod
	def _handle_addl(cls, addl):
		pass
	@classmethod
	def _handle_args(cls, **kwargs):
		pass

	def apply(self, tracker, emit):
		pass

	@staticmethod
	def register(clsSub):
		Instruction.types.append(clsSub)
		if clsSub:
			Instruction.name_map[clsSub.name] = clsSub
			setattr(Instruction, clsSub.name, clsSub) # Create reverse lookup

