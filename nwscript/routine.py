import sys
from nwscript.instructions import Instruction, ExecutionTracker
from bytecode.routine import Routine as RoutineBase
from collections import deque

class Routine(RoutineBase):
	def __init__(self, lines, line_start, line_end):
		self._lines = lines
		self.bounds = (line_start, line_end)
		self.offset = lines[line_start].offset
		self.args = None
		self.return_type = None
		self.stack_start = []
	def instructions(self):
		ls, le = self.bounds
		return self._lines[ls : (le + 1)]

	def has_signature(self):
		return (self.args is not None)

	def emit_wrapper(self, write, name, type_return, types_arg, generate_internal):
		ls, le = self.bounds
		write('{} {}({}) {{\t\t//to {:08X}\n'.format((type_return.keyword if type_return else 'void'), name, (','.join(t.keyword for t in types_arg)), self._lines[le].offset))
		# Evaluate the Stack to determine the arguments/return types
		generate_internal(self.instructions())
		write('}\n')

	@staticmethod
	def all_from_instructions(insts, map_lines = None):
		routine_start = Routine(insts, 0, None)
		return routine_start.all_subroutines(map_lines)

	def all_subroutines(self, map_lines = None):
		class BranchTracker(object):
			def __init__(self, items = []):
				self._branches_stack = deque(items)
				self._branches_processed = set(items)
			def append(self, offset, stack):
				if offset not in self._branches_processed:
					self._branches_stack.append((offset, stack))
					self._branches_processed.add(offset)
				return (offset, stack)
			def pop(self):
				return self._branches_stack.pop()
			def __len__(self):
				return len(self._branches_stack)

		# We want to determine the function signatures of all of the routines in the code
		line_start, line_end = self.bounds
		routines_processed = {}
		routines_stack = deque([(line_start, self, BranchTracker())]) # Start at line 0
		tracker = ExecutionTracker()
		tracker.block_stack = None

		# Assumptions:
		#   Any recursion will always have a path that does not invoke the recursion
		#     (otherwise, we will hit an infinite loop).
		#     Example: If routine A calls routine B, which then calls routine A,
		#     either routine A or routine B will have a path (with JZ/JNZ) that
		#     will not cause recursion.
		#   A function will have set the stack appropriately (popped all parameters,
		#     set the return variable) by the time we encounter any RETN instruction.
		#   Jump Instructions (JZ, JNZ, JMP) will stay within their Subroutines.

		class SubroutineBreak(Exception):
			pass
		def try_next_branch(branches, routine, inst):
			routines_stack.append((0, routine, branches))
			while len(routines_stack):
				l, r, branches = routines_stack.pop()
				if len(branches):
					offset, stack = branches.pop()
					tracker.stack_local = stack
					routines_stack.append((map_lines[offset], r, branches))
					raise SubroutineBreak()
			raise Exception('Infinite loop in Routine {:08X}, {}: {:08X}'.format(routine.offset, inst.name, inst.offset))


		def handle_Subroutine(routine, inst, branches):
			offset_jump = inst.jump_offset()
			line_jump = map_lines[offset_jump]
			r_new = routines_processed.get(offset_jump, None)
			if r_new is None:
				r_new = Routine(self._lines, line_jump, None)
				r_new.stack_start = list(tracker.stack_local) # Make a copy
				routines_stack.append((map_lines[inst.offset] + 1, routine, branches)) # Resume at next line
				routines_stack.append((line_jump, r_new, BranchTracker()))
				raise SubroutineBreak()
			else:
				if r_new.args is None:
					eprint('Found recursive call to fn_{:08X} at {:08X}'.format(r_new.offset, inst.offset))
					try_next_branch(branches, routine, inst)
				else: # We've already processed the Routine
					assert len(r_new.args) <= len(tracker.stack_local), (
						'fn_{:08X} expects {} args, but stack has only {}'
							.format(r_new.offset, len(r_new.args), len(tracker.stack_local)))
					for i, t_arg in enumerate(r_new.args):
						t, v = tracker.stack_local.pop()
						if t_arg != t:
							eprint('Argument {} type mismatch in Stack: {} (signature {})'.format(i, t.name, t_arg.name))

		def handle_Return(routine, inst, branches):
			# Evaluate stack and determine parameters
			if routine.args is None:
				assert len(routine.stack_start) >= len(tracker.stack_local), (
					'Stack started smaller than result of Routine: {:08X} (started: {}, resulted: {}, RETN: {:08X})'
						.format(routine.offset, len(routine.stack_start), len(tracker.stack_local), inst.offset))
				routine.args = [t for t, v in reversed(routine.stack_start[len(tracker.stack_local):])]
				if len(tracker.stack_local) > 0:
					t, v = routine.stack_start[len(tracker.stack_local) - 1]
					t_end, v_end = tracker.stack_local[-1]
					if v != v_end: # We expect that the Routine has modified the Return value (presumably from None to a Value)
						routine.return_type = t
			else:
				assert len(routine.stack_start) == (len(tracker.stack_local) + len(routine.args)), (
					'Mismatch between Routine ({:08X} at RETN {:08X}) signature ({} args) and Stack ({} args)'
						.format(routine.offset, inst.offset, len(routine.args), (len(routine.stack_start) - len(tracker.stack_local))))
			ls, le = routine.bounds
			if inst.offset > (le or 0): # The furthest RETN Instruction marks the end of the Routine
				routine.bounds = (ls, map_lines[inst.offset])
			# We want to process the rest of the Routine just to find the bounds and any other Subroutines
			if len(branches):
				try_next_branch(branches, routine, inst)
			raise SubroutineBreak()

		def handle_Condition(routine, inst, branches):
			# Each Condition has two paths; we must take one
			#   but store the other one to take it later.
			jump_offset = inst.jump_offset()
			if jump_offset > inst.offset:
				branches.append(jump_offset, deque(tracker.stack_local)) # Copy of stack

		def handle_Jump(routine, inst, branches):
			jump_offset = inst.jump_offset()
			if jump_offset > inst.offset:
				return jump_offset
			# For backwards jumps, we consider that a dead end in trying to find the routine exit
			try_next_branch(branches, routine, inst)

		handle_None = lambda *args: -1
		handle_inst = {
			'JSR':  handle_Subroutine,
			'JZ':   handle_Condition,
			'JNZ':  handle_Condition,
			'JMP':  handle_Jump,
			'RETN': handle_Return,
		}

		while len(routines_stack):
			while len(routines_stack):
				line, r, branches_stack = routines_stack.pop()
				routines_processed[r.offset] = r
				skip_to = 0
				for inst in self._lines[line:]:
					if inst.offset < skip_to:
						continue # Skip

					# Apply Instruction to Stack before seeing any branching
					#   so that when we copy the Stack, we have it in the state
					#   following the Instruction.
					inst.apply(tracker, lambda s: s) # For 'emit', do nothing

					try:
						skip_to = handle_inst.get(inst.name, handle_None)(r, inst, branches_stack)
					except SubroutineBreak:
						break

			for r in (r for o, r in routines_processed.iteritems() if r.args is None):
				ls, le = r.bounds
				routines_stack.append((ls, r, BranchTracker()))
				tracker.stack_local = r.stack_start
				break # Just find first Routine missing bounds
		return routines_processed

