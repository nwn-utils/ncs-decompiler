void TestRecursive(int i) {
    if (i++ < 5)
        TestRecursive(i);
}

void main() {
    TestRecursive(0);
}

