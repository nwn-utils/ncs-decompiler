import sys
import codecs
import json
import struct
from nwscript.instructions import Instruction, ExecutionTracker, InvalidFormatError, InvalidInstructionError, hexval, read_int, to_int32
from nwscript.routine import Routine
from collections import deque

def eprint(s):
	sys.stderr.write(s + '\n')

def add_to_array(a, pos, val, valDefault):
	if (pos < len(a)):
		a[pos] = val
		return
	for i in range(len(a), pos):
		a.append(valDefault) # Inflate with default value
	a.append(val)

def list_to_array(l, valDefault):
	a = []
	for i, v in l:
		add_to_array(a, i, v, valDefault)
	return a

def main(fIn, fOut, file_nws, pcode):
	try:
		decompile(fIn, fOut, file_nws, pcode)
	except InvalidFormatError:
		eprint('Could not parse input; ensure valid {} data'.format('PCODE' if pcode else 'NCS'))
	except InvalidInstructionError as ex:
		eprint('{}\n\t{}'.format(ex.message, ex.Instruction))

def decompile(fIn, fOut, file_nws, pcode):
	if file_nws:
		with file_nws as fNWS:
			Instruction.extend(fNWS.read())

	# Read all lines in fIn, into offset,data tuples
	lines = [Instruction.from_line(l.strip()) for l in fIn.readlines()] if pcode else read_from_binary(fIn)
	map_lines = {inst.offset:i for (i, inst) in enumerate(lines)} # We need this for quick lookup of destinations of jumps and subroutines

	routine_start = Routine(lines, 0, None)
	routines = routine_start.all_subroutines(map_lines)

	routines_skip = [routine_start]
	globals_or_main = routines[next(i.jump_offset() for i in routine_start.instructions() if i.name == 'JSR')]
	vars_global = []
	routine_main = globals_or_main
	for inst_Global in (i for i in globals_or_main.instructions() if i.name == 'SAVEBP'): # The Globals area contains the SAVEBP Instruction
		routines_skip.append(globals_or_main)
		tracker = ExecutionTracker()
		for inst in globals_or_main.instructions():
			inst.apply(tracker, lambda s: None)
			if inst == inst_Global:
				vars_global = list(tracker.globals)
			if inst.name == 'JSR':
				routine_main = routines[inst.jump_offset()]
		for i, (t, v) in enumerate(vars_global):
			fOut.write('{} {}{};\n'.format(t.keyword, t.initial + str(i) + '_g', '' if hasattr(v, 'name_with_scope') else (' = {}'.format(v))))
		fOut.write('\n')
		break
	routines_skip = set(routines_skip)

	for routine in (r for o, r in routines.iteritems() if ((r not in routines_skip) and (r != routine_main))):
		s_params = 'Unable to determine signature' if routine.args is None else (', '.join('{} v{}'.format(t.keyword, i) for i, t in enumerate(routine.args)))
		fOut.write('{} fn_{:08X}({});\n'.format(('void' if routine.return_type is None else routine.return_type.keyword), routine.offset, s_params))
	fOut.write('\n')

	for routine in (r for o, r in routines.iteritems() if r not in routines_skip):
		# Build Stack and emit code
		tracker = ExecutionTracker(vars_global)
		tracker.routines_table = routines
		tracker.set_function(routine.args)
		args_with_name = list(reversed(tracker.stack_local))

		def emit_line(s):
			fOut.write(s + '\n')
		def emit_instructions(insts):
			for inst in insts:
				if inst.offset >= tracker.skip_until:
					while (len(tracker.block_stack) > 0):
						oS, oE, stack = tracker.block_stack[-1]
						if (inst.offset < oE):
							break
						emit_line(('\t' * len(tracker.block_stack)) + '}')
						tracker.block_stack.pop()
					inst.apply(tracker, lambda s: emit_line(('\t' * (len(tracker.block_stack) + 1)) + s)) # Track indentation state
			emit_line('\n///* ------ Instructions ------- */')
			for inst in insts:
				emit_line('//{}'.format(inst))
		ls, le = routine.bounds
		emit_line('\n\n{} {}({}) {{\t\t//to {:08X}'
			.format((routine.return_type.keyword if routine.return_type else 'void')
				, 'main' if routine == routine_main else 'fn_{:08X}'.format(lines[ls].offset)
				, (', '.join('{} {}'.format(t.keyword, v.name_with_scope()) for t, v in args_with_name))
				, lines[le].offset))
		# Evaluate the Stack to determine the arguments/return types
		emit_instructions(routine.instructions())
		emit_line('}')

def read_from_binary(fIn):
	class bin_read_wrapper(object):
		def __init__(self, fBin):
			self.pos = 0
			self.fBin = fBin
		def read(self, length):
			val = self.fBin.read(length)
			self.pos = self.pos + length
			return val
	fBin = bin_read_wrapper(fIn)

	version = fBin.read(8)
	insts = []
	while True:
		offset = fBin.pos
		opcode = fBin.read(1)
		if not opcode:
			break
		try:
			inst = Instruction.types[read_int(opcode)](offset)
			inst.read(fBin)
		except (KeyError, ValueError, TypeError), ex:
			eprint('Unable to parse instruction (0x{:02X}) at offset 0x{:08X}'.format(read_int(opcode), offset))
			eprint(str(ex))
			raise InvalidFormatError()
		insts.append(inst)
	return insts

if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser(description="""Decompile """)
	parser.add_argument('-i', '--input', dest='infile', type=argparse.FileType('rb'), default=sys.stdin, help="""Read from FILE (default to STDIN if no file)""", metavar='FILE')
	parser.add_argument('-o', '--output', dest='outfile', type=argparse.FileType('wb'), default=sys.stdout, help="""Output to FILE instead of STDOUT""", metavar='FILE')
	#parser.add_argument('-p', '--from-pcode', dest='from_pcode', action='store_true', help="""Parse PCODE input rather than binary (NCS) format""")
	parser.add_argument('-n', '--nwscript.nss', dest='nwsfile', type=argparse.FileType('rb'), default=None, help="""Decompile against NWScript.nss contents in FILE""", metavar='FILE')
	args = parser.parse_args()
	args.from_pcode = False

	#get_NWS = (lambda: codecs.getreader('utf-8')(sys.stdin if args.nwsfile == '-' else open(args.nwsfile, 'rb'))) if args.nwsfile else None
	main(args.infile, args.outfile, args.nwsfile, args.from_pcode)

