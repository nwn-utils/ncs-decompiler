import ply.lex

def lex():
	reserved = {
		'#define'  : 'DEFINE',
		'#include' : 'INCLUDE',
		'if'       : 'IF',
		'else'     : 'ELSE',
		'while'    : 'WHILE',
	}
	tokens = [
		'ID',
		'HASHKEY',
		'INTEGER',
		'FLOAT',
		'STRING',
		'COMMENT',
		'OP_EQUALITY',
		'OP_COMPARISON',
	] + list(reserved.values())

	literals = '+-*/;=(){}[]",.'

	t_OP_EQUALITY   = r'[!=]='
	t_OP_COMPARISON = r'[><]=?'

	class Identifier(object):
		def __init__(self, name):
			self.name = name
	def t_HASHKEY(t):
		r'\#[_a-zA-Z]\w*'
		t.type = reserved[t.value] # Will raise a KeyError if the value doesn't match a keyword
		return t
	def t_ID(t):
		r'[_a-zA-Z]\w*'
		t.type = reserved.get(t.value, 'ID')
		if t.type == 'ID':
			t.value = Identifier(t.value)
		return t

	def t_FLOAT(t):
		r'-?\d+[.]\d+f?' # Float values can end with 'f'
		if t.value[-1] == 'f':
			t.value = t.value[0:-1]
		t.value = float(t.value)
		return t
	def t_INTEGER(t):
		r'-?\d+'
		t.value = int(t.value)
		return t

	def t_STRING(t):
		r'\"(([^"]|\\[\/nt"])*)\"' # Not sure if this successfully matches only well-formed string literals
		t.value = t.value.decode('string-escape')
		return t

	def t_COMMENT(t):
		r'(/\*(.|\n)*\*/)|(//.*)'
		pass # Discard token

	def t_newline(t):
		r'\n+'
		t.lexer.lineno += len(t.value)

	t_ignore = ' \t\r' # Ignore spaces and tabs; we may eventually not want to do this


	def t_error(t):
		class LexerError(Exception):
			pass
		raise LexerError('Illegal input: "{}" at line {}'.format(t.value, t.lexer.lineno));


	lexer = ply.lex.lex()
	lexer.tokens = tokens
	return lexer
