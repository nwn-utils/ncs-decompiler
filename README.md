# NCS Decompiler

This project aims to take compiled NCS code and output a representation of it in NWScript (NSS).

This should work for any game that uses NWScript, not just Neverwinter Nights, but games like Star Wars Knight of the Old Republic, KotOR 2, and The Witcher

## To run (Python 2.7):

    python 'NCS Decompile.py' -n path/to/nwscript.nss < path/to/input.ncs > path/to/output.nss

## Required Python libraries:
 - [ply](https://pypi.org/project/ply/): I really wanted to have no external dependencies, but without writing my own parser, I had to rely on this package. But, my Python installation already had pre-installed it, so I had some confidence that it has good support.

## Known Issues:
 - Most NWScript compilers I've used try to find the nwscript.nss file from your environment (detecting via the Windows registry) or at least can extract it if you provide the game installation directory. For now, this project requires you to provide the nwscript.nss file.
   - This file contains the definitions for the extra Engine Types and available Actions.
   - For NWN, you can use a tool like [NWN Explorer](http://neverwintervault.org/project/nwn1/other/tool/nwn-explorer-reborn) to extract the nwscript.nss file from the game sources.
 - This won't output scripts which start with `StartingConditional` instead of `main` properly; it will probably output `int main()` and may even complain about Stack mismatches.
 - This doesn't handle `struct` and `vector` variables well, if at all.
 - The output logic does have holes, and so right now can give a general sense of the code, though an imperfect representation.
   - For example, it doesn't output `while` statements. But it will output an `if` statement, and then a comment at the end that indicates that it has actually detected a `while` pattern.
   - Future development will work to improve on the block output.
     - Eventually, I may want to recreate an Abstract Syntax Tree (AST) and then emit the code from there, establishing a third pass in the decompilation routines.
 - This project already contains a lexer and parser for NWScript, so presumably include a compiler as well, but [good open-source compilers already exist](https://gitlab.com/glorwinger/nwnsc), so the inclusion of a compiler in this project doesn't take as high a priority.
 - With Python 2's End-of-Life approaching in 2020, we will eventually need to move this to Python 3.

## Resources:
 - I rely heavily on [the extremely helpful NCS file reference guide](http://www.nynaeve.net/Skywing/nwn2/documentation/ncs.html).

