import sys
import struct
import parser
from bytecode.instructions import Instruction as InstBase, ExecutionTracker as TrackerBase, InvalidInstructionError
from collections import deque

def eprint(s):
	sys.stderr.write(s + '\n')

def to_int32(val):
	return (((val + 0x80000000) & 0xFFFFFFFF) - 0x80000000) # Only deal with last 32 bits

def read_int(source, size=4):
	s = source.read(size) if hasattr(source, 'read') else source
	val, = struct.unpack('>i', (s[0:4] if len(s) >= 4 else (struct.pack('x' * (4 - len(s)))+s)))
	return to_int32(val) # Pad to 4 bytes

def hexval(s): # Signed 32-bit hex value
	return to_int32(int(s, 16)) # Unsigned

def add_to_array(a, pos, val, valDefault):
	if (pos < len(a)):
		a[pos] = val
		return
	for i in range(len(a), pos):
		a.append(valDefault) # Inflate with default value
	a.append(val)

def list_to_array(l, valDefault):
	a = []
	for i, v in l:
		add_to_array(a, i, v, valDefault)
	return a			

class InvalidFormatError(Exception):
	pass

class ExecutionVariable(object):
	def __init__(self, prefix, name, scope):
		self.prefix = prefix
		self.name = name
		self.scope = scope
	def name_with_scope(self):
		return ('{}{}_{}'.format(self.prefix, self.name, self.scope))

class ExecutionTracker(TrackerBase):
	def __init__(self, vars_global = []):
		self.stack_local = []
		self.globals = vars_global
		self.args_function = None
		self.counter_var = 0
		self.block_stack = []
		self.skip_until = -1
		self.routines_table = None

	def _new_var(self, t, scope):
		v = ExecutionVariable((t.initial or 'X') if t else 'V', str(self.counter_var), scope) # Need a better way to mark a Variable as unknown, and then to revisit it later
		self.counter_var = self.counter_var + 1
		return v

	@staticmethod
	def _arg_mismatch(t, t_expect):
		eprint('Argument type mismatch: Expected {}, received {}'.format(t_expect.name, t.name))

	def _index_var(self, pos, list_var, name_type = 'Variable'):
		offset = pos
		for i, (t, v) in enumerate(list_var):
			offset = offset + (t.stack_size if t else 4) # TODO: Engine types
			if offset >= 0:
				if offset != 0:
					eprint('Incorrect addressing of {}: {:08X}'.format(name_type, pos))
				return i
		return offset

	def _index_local_or_fn(self, pos, t_expect):
		i_local = self._index_var(pos, reversed(self.stack_local))
		if i_local < 0:
			return None
		l, i = self.stack_local, (len(self.stack_local) - i_local - 1) # Because we reversed
		t, v = l[i]
		if t and t_expect and (t != t_expect):
			self._arg_mismatch(t, t_expect)
		return (i, l)

	def get(self, pos, t_expect = None):
		i, l = self._index_local_or_fn(pos, t_expect)
		return l[i]
	def get_top(self):
		return self.stack_local[-1]

	def set(self, pos, val, t_expect):
		idx = self._index_local_or_fn(pos, t_expect)
		if idx is None:
			return None # Must be the return statement
		i, l = idx
		t, v = l[i]
		l[i] = (t, val)
		return l[i]

	def pop(self, t_expect = None):
		t, v = self.stack_local.pop()
		if not t:
			t = t_expect
		elif t_expect and (t != t_expect):
			self._arg_mismatch(t, t_expect)
		return (t, v)

	def push(self, t, v = None):
		var_new = (t, (self._new_var(t, 'l') if v is None else v))
		self.stack_local.append(var_new)
		return var_new

	def contains(self, t, v):
		return (t, v) in self.stack_local

	def locals_to_globals(self):
		self.globals = list(self.stack_local)
		self.stack_local = deque()
	def globals_to_locals(self):
		if len(self.stack_local):
			eprint('Restore BP for non-empty variable stack')
		self.stack_local = deque(self.globals)
		self.globals = []

	def _index_global(self, pos):
		i = self._index_var(pos, enumerate(self.globals), 'Global')
		if i < 0:
			eprint('Failed to find Global at {:08X}'.format(pos))
		return i
	def get_global(self, pos):
		i = self._index_global(pos)
		return None if i < 0 else self.globals[i]
	def set_global(self, pos, v):
		i = self._index_global(pos)
		if i < 0:
			return None
		# Ensure types match?
		self.globals[i][1] = v
		return self.globals[i]
	def set_function(self, types):
		self.stack_local = [(t, self._new_var(t, 'r')) for t in reversed(types)]

class Instruction(InstBase):
	_size_static = 0 # Default
	@classmethod
	def from_line(cls, s):
		offset, opcode, args = tuple(s[0:34].strip().split(' ', 2))
		code = hexval(opcode)
		clsInst = (cls.types[code] if (code < len(cls.types)) else cls) or cls

		inst = clsInst(hexval(offset))
		if (cls == clsInst):
			inst.name = opcode
			inst.code = code
		inst.parse_line_args(args.split(' ', 1), s) if (len(args) > 1) else ''
		inst.line = s
		return inst

	def parse_line_args(self, args, line):
		self.arg_type = self.arg_types[hexval(args[0])]
		self.args = struct.pack('>i', hexval(args[1].replace(' ', ''))) if (len(args) > 1) else ''

	def read_type(self, fBin):
		self.arg_type = Instruction.arg_types[read_int(fBin, 1)]
	def read(self, fBin):
		self.read_type(fBin)
		self.args = fBin.read(self._size_static) if (self._size_static > 0) else ''

	def __str__(self):
		args_string = self.args_string()
		return ('{:08X}\t{}'.format(self.offset, self.name)
			+ (
				' ?' if self.arg_type is None
				else (
					(' (' + ','.join([t.keyword for t in self.arg_type]) + ')') if len(self.arg_type) > 0 else ''
				)
			)
			+ ((': ' + args_string) if args_string else '')
		)
	def __repr__(self):
		data = deque([('offset', '0x{:08X}'.format(self.offset))])
		data.append(('name', "'{}'".format(self.name.encode('string-escape'))))
		if (self._size_static > 0):
			data.append(('args', '0x' + ''.join('{:02X}'.format(read_int(v, 1)) for v in self.args)))
		if (len(self.arg_type) > 0):
			data.append(('arg_type', repr(self.arg_type)))
		return '{' + ','.join(("'{}':{}".format(n, v) for (n, v) in data)) + '}'

	def args_string(self):
		if (self._size_static > 0):
			return '0x{:08X}'.format(read_int(self.args))
		else:
			return ''

	def type_return(self):
		return (self.arg_type[0] if len(self.arg_type) > 0 else None)

def _extend_Instruction():
	def is_var(v):
		return hasattr(v, 'name_with_scope')
	def out_var(v):
		return (v.name_with_scope() if is_var(v) else v)

	##########################
	# Define Argument Types
	##########################
	class InstructionArgType(object):
		stack_size = 4
		def __init__(self, name, keyword, initial, convert_bytestream = None, to_arg_from_val = (lambda v: v)):
			self.name = name
			self.keyword = keyword
			self.initial = initial
			self.convert_bytestream = convert_bytestream or (lambda b: read_int(b))
			self.to_arg = (lambda b: to_arg_from_val(self.convert_bytestream(b)))
	Instruction.arg_types = []

	class InstructionArgTypeMapper(object):
		m = {} # 'map' is reserved
		by_keyword = {}
		def register(self, name, keyword, *args):
			a = InstructionArgType(name, keyword, *args)
			arg_type.m[name] = a
			arg_type.by_keyword[keyword] = a
			setattr(arg_type, name, a)
			return a
	arg_type = InstructionArgTypeMapper()
	for t in [
		('Integer', 'int', 'I'),
		('Float', 'float', 'F', lambda b: struct.unpack('>f', b)[0]),
		('String', 'string', 'S', (lambda b: b), (lambda v: '"' + v + '"')), # TODO: Escape Quotes
		('Object', 'object', 'O', None, (lambda v: 'OBJECT_SELF' if v == 0 else 'OBJECT_INVALID' if v == 1 else v)),
		('Structure', 'struct', 'T'),
		('Vector', 'vector', 'V'),
		# Additional Engine types
	]:
		arg_type.register(*t)

	for code, t in [
		(0x00, []),
		(0x01, []),
		(0x03, 'Integer'),
		(0x04, 'Float'),
		(0x05, 'String'),
		(0x06, 'Object'),
		# Engine Types
		(0x20, ('Integer', 'Integer')),
		(0x21, ('Float', 'Float')),
		(0x22, ('Object', 'Object')),
		(0x23, ('String', 'String')),
		(0x24, ('Structure', 'Structure')),
		(0x25, ('Integer', 'Float')),
		(0x26, ('Float', 'Integer')),
		# Engine Types
		(0x3A, ('Vector', 'Vector')),
		(0x3B, ('Vector', 'Float')),
		(0x3C, ('Float', 'Vector')),
	]:
		arg_types = [arg_type.m[name_type] for name_type in ([t] if isinstance(t, basestring) else t)]
		add_to_array(Instruction.arg_types, code, arg_types, None)

	############################
	# Define Instruction Types
	############################
	class InstructionAssignRelative(Instruction):
		_size_static = 6

		def get_offset(self):
			return read_int(self.args[0:4])
		def get_bytes(self):
			return read_int(self.args[4:])

		def args_string(self):
			return '0x{:08X}, {}'.format(read_int(self.args[0:4]), read_int(self.args[4:]))

	class InstructionCPDOWNSP(InstructionAssignRelative):
		name = 'CPDOWNSP'
		code = 0x01
		def apply(self, tracker, emit):
			t, v = tracker.get_top()
			idx = tracker.set(self.get_offset(), v, t)
			if idx is None:
				emit('return {};'.format(out_var(v)))

	class InstructionCPTOPSP(InstructionAssignRelative):
		name = 'CPTOPSP'
		code = 0x03
		def apply(self, tracker, emit):
			try:
				t, v = tracker.get(self.get_offset())
			except TypeError:
				raise Exception('Attempt to access position {} beyond stack scope at {:08X}'.format(self.get_offset(), self.offset))
			tracker.push(t, v)

	class InstructionRSADD(Instruction):
		name = 'RSADD'
		code = 0x02
		def read_type(self, fBin):
			self.arg_code = read_int(fBin, 1)
			self.arg_type = Instruction.arg_types[self.arg_code]
		def apply(self, tracker, emit):
			if self.arg_type:
				t, v = tracker.push(self.arg_type[0])
			else:
				eprint('RS for Engine Type {:08X}'.format(self.offset))
				t, v = tracker.push(arg_type.Object)
			emit('{} {};'.format(t.keyword, v.name_with_scope()))

	class InstructionCONST(Instruction):
		name = 'CONST'
		code = 0x04

		def read(self, fBin):
			self.read_type(fBin)
			if (self.arg_type[0] == arg_type.String):
				size = read_int(fBin, 2)
				self.args = fBin.read(size)
			else:
				self.args = fBin.read(4)

		def args_string(self):
			def output_object(a):
				addr = read_int(a)
				return ('OBJECT_SELF' if addr == 0 else '0x{:08X}'.format(addr))
			handle_arg = {
				arg_type.String:  lambda a: '0x{:04X} {}'.format(len(a), a),
				arg_type.Integer: lambda a: '{}'.format(read_int(a)),
				arg_type.Float:   lambda a: '{}'.format(struct.unpack('>f', a)[0]),
				arg_type.Object:  output_object,
			}
			return str(self.arg_type[0].to_arg(self.args))

		def parse_line_args(self, args, line):
			self.arg_type = Instruction.arg_types[hexval(args[0])]
			if (self.arg_type[0] == arg_type.String):
				self.args = line[41:]
			else:
				super(InstructionCONST, self).parse_line_args(args, line)

		def apply(self, tracker, emit):
			t = self.arg_type[0]
			tracker.push(t, t.to_arg(self.args))

	class InstructionOperator(Instruction):
		_size_static = 0
		def apply(self, tracker, emit): # Default to two-argument, one return Instruction
			t1, v1 = tracker.pop(self.arg_type[0])
			t2, v2 = tracker.pop(self.arg_type[1])
			emit('{} = {} {} {};'.format(self.get_return(tracker, t2, v2), out_var(v2), self.op_string, out_var(v1)))
		def type_return(self):
			return self.arg_type[0]
		def get_return(self, tracker, t0, v0):
			reuse_var = (v0 if (is_var(v0) and (self.type_return() == t0) and (not tracker.contains(t0, v0))) else None)
			t, v = tracker.push(self.type_return(), reuse_var)
			return ('' if reuse_var else (t.keyword + ' ')) + out_var(v)

	class InstructionACTION(InstructionOperator): # This one requires parsing nwscript.nss
		name = 'ACTION'
		code = 0x05
		functions = []
		_size_static = 3
		def args_string(self):
			return ('0x{:04X} ({})'.format(read_int(self.args[0:2]), read_int(self.args[2:])))

		def get_function(self):
			id_func = read_int(self.args[0:2])
			f = self.functions[id_func] if id_func < len(self.functions) else None
			if not f:
				raise self.create_error('Missing ACTION: 0x{:04X} (Have you imported the extension Actions?)'.format(id_func))
			return f
			
		def apply(self, tracker, emit):
			args = []
			for i in range(0, read_int(self.args[2:])):
				args.append(tracker.pop())
			fACTION = self.get_function()
			s_out = '{}({});'.format(fACTION.name, ', '.join(str(out_var(v)) for t, v in args))
			t_ret = self.type_return()
			if t_ret:
				if len(args) > 0:
					t, v = args[-1]
					s_assign = self.get_return(tracker, t, v)
				else:
					s_assign = self.get_return(tracker, None, None)
				s_out = '{} = '.format(s_assign) + s_out
			emit(s_out)
		def type_return(self):
			f = self.get_function()
			return arg_type.by_keyword.get(f.type_return, None)

	class InstructionOpBoolean(InstructionOperator):
		def type_return(self):
			return arg_type.Integer

	class InstructionLOGAND(InstructionOpBoolean):
		op_string = '&&'
	class InstructionLOGOR(InstructionOpBoolean):
		op_string = '||'
	class InstructionINCOR(InstructionOpBoolean):
		op_string = '|'
	class InstructionEXCOR(InstructionOpBoolean):
		op_string = 'XOR'
	class InstructionBOOLAND(InstructionOpBoolean):
		op_string = '&'

	class InstructionOperatorWithStruct(InstructionOpBoolean):
		def read(self, fBin):
			self.read_type(fBin)
			if (self.arg_type[0] == arg_type.Structure):
				self.args = fBin.read(2)
				size = read_int(self.args)
		def args_string(self):
			if (self.arg_type[0] == arg_type.Structure):
				return ('{}'.format(read_int(self.args)))
			else:
				return super(InstructionOperatorWithStruct, self).args_string()
	class InstructionEQUAL(InstructionOperatorWithStruct):
		op_string = '=='
	class InstructionNEQUAL(InstructionOperatorWithStruct):
		op_string = '!='
	class InstructionGEQ(InstructionOpBoolean):
		op_string = '>='
	class InstructionGT(InstructionOpBoolean):
		op_string = '>'
	class InstructionLT(InstructionOpBoolean):
		op_string = '<'
	class InstructionLEQ(InstructionOpBoolean):
		op_string = '<='
	class InstructionSHLEFT(InstructionOperator):
		op_string = '<<'
	class InstructionSHRIGHT(InstructionOperator):
		op_string = '>>'
	class InstructionUSHRIGHT(InstructionOperator): # Unsigned Shift
		op_string = '>>>'
	class InstructionADD(InstructionOperator):
		op_string = '+'
	class InstructionSUB(InstructionOperator):
		op_string = '-'
	class InstructionMUL(InstructionOperator):
		op_string = '*'
	class InstructionDIV(InstructionOperator):
		op_string = '/'
	class InstructionMOD(InstructionOperator):
		op_string = '%'
	class InstructionOpUnary(InstructionOperator):
		def apply(self, tracker, emit):
			t1, v1 = tracker.pop(self.arg_type[0])
			emit('{} = {} {};'.format(self.get_return(tracker, t1, v1), self.op_string, out_var(v1)))
	class InstructionNEG(InstructionOpUnary):
		op_string = '-'
	class InstructionCOMP(InstructionOpUnary):
		op_string = '~' # Correct?
	class InstructionNOT(InstructionOpUnary):
		op_string = '!'


	class InstructionMOVSP(Instruction): # Move Stack Pointer, thereby clearing variables from Stack
		_size_static = 4
		def apply(self, tracker, emit):
			offset = read_int(self.args)
			while offset < 0:
				try:
					t, v = tracker.pop()
					offset = offset + (t.stack_size if t else 4) # TODO: Engine types
				except IndexError:
					eprint('Popped from empty stack at {:08X} ({})'.format(self.offset, offset))
					break

	class InstructionSTORE_STATEALL(Instruction): # Obsolete
		pass

	class InstructionJump(Instruction):
		_size_static = 4
		_jump_prefix = ':'

		def jump_offset(self):
			return self.offset + read_int(self.args)
		
		def args_string(self):
			return '{}{:08X}'.format(self._jump_prefix, self.jump_offset())

	class InstructionJSR(InstructionJump):
		name = 'JSR'
		code = 0x1E
		_jump_prefix = 'fn_'
		def apply(self, tracker, emit):
			if tracker.routines_table is None:
				return # We must be in the phase of compiling the signatures
			routine = tracker.routines_table[self.jump_offset()]
			s_invocation = 'fn_{:08X}({});'.format(self.jump_offset(), ', '.join(str(out_var(v)) for t, v in (tracker.pop(t_a) for t_a in routine.args)))
			if routine.return_type is not None:
				t_ret, v_ret = tracker.get_top()
				if t_ret != routine.return_type:
					eprint('Return type mismatch for Routine {:08X} at offset 0x{:08X}'.format(routine.offset, self.offset))
				s_invocation = '{} = '.format(out_var(v_ret)) + s_invocation
			emit(s_invocation)

	class InstructionJMP(InstructionJump):
		name = 'JMP'
		code = 0x1D
		def apply(self, tracker, emit):
			offset_new = self.jump_offset()
			if offset_new == (self.offset + self._size_static + 2):
				return # Essentially a non-operation (Jump to next Instruction)
			offset_skip = offset_new
			if (self.offset > offset_new): # We have a WHILE loop
				emit('// WHILE loop')
				return
			if tracker.block_stack and len(tracker.block_stack) > 0:
				oS, oE, stack = tracker.block_stack.pop()
				if offset_new < oE:
					tracker.block_stack.append((oS, oE, stack))
				elif offset_new > oE and stack is not None:
					offset_skip = oE
					emit('}} else {{ // {:08X}'.format(self.offset))
					tracker.stack_local = stack # Re-set stack to beginning of block state
					tracker.block_stack.append((oE, offset_new, None))
				else:
					emit('}')
			tracker.skip_until = offset_skip

	class InstructionJumpConditional(InstructionJump):
		op_test = ''
		def apply(self, tracker, emit):
			t, v = tracker.pop()
			if t != arg_type.Integer:
				eprint('Non-Integer ({}) Jump comparison {:08X}'.format((t.name if t else 'NONE'), self.offset))
			emit('if ({}{}) {{ // {:08X}'.format(self.op_test, v.name_with_scope() if hasattr(v, 'name_with_scope') else v, self.offset))
			if tracker.block_stack is not None:
				tracker.block_stack.append((self.offset, self.jump_offset(), deque(tracker.stack_local)))
	class InstructionJZ(InstructionJumpConditional):
		name = 'JZ'
		code = 0x1F

	class InstructionJNZ(InstructionJumpConditional):
		name = 'JNZ'
		code = 0x25
		op_test = '!'

	class InstructionRETN(Instruction):
		name = 'RETN'
		code = 0x20

	class InstructionDESTRUCT(Instruction):
		_size_static = 6

	class InstructionCPDOWNBP(InstructionAssignRelative): # Set Global (offsets relative to BP point to Globals)
		name = 'CPDOWNBP'
		code = 0x26
		def apply(self, tracker, emit):
			t, v = tracker.get_top()
			tracker.set_global(self.get_offset(), v)

	class InstructionCPTOPBP(InstructionAssignRelative): # Retrieve Global
		name = 'CPTOPBP'
		code = 0x27
		def apply(self, tracker, emit):
			t, v = tracker.get_global(self.get_offset())
			tracker.push(t, v)

	class InstructionModPointer(Instruction):
		_size_static = 4
		def _get_var(self, tracker, pos):
			return tracker.get(pos)
		_op = '++'

		def apply(self, tracker, emit):
			t, v = self._get_var(tracker, read_int(self.args[0:4]))
			emit('{}{};'.format(out_var(v), self._op))
	class InstructionINCSP(InstructionModPointer):
		pass
	class InstructionDECSP(InstructionModPointer):
		_op = '--'
	class InstructionINCBP(InstructionModPointer):
		def _get_var(self, tracker, pos):
			return tracker.get_global(pos)
	class InstructionDECBP(InstructionModPointer):
		_op = '--'
		def _get_var(self, tracker, pos):
			return tracker.get_global(pos)

	class InstructionSAVEBP(Instruction):
		def apply(self, tracker, emit):
			tracker.locals_to_globals()

	class InstructionRESTOREBP(Instruction):
		def apply(self, tracker, emit):
			tracker.globals_to_locals()

	class InstructionSTORE_STATE(Instruction): # Defer execution, like for a DelayCommand or Action enqueue
		_size_static = 8

	class InstructionNOP(Instruction):
		pass

	class InstructionT(Instruction):
		name = 'T'
		code = 0x42
		def read(self, fBin):
			self.size = read_int(fBin)
		def parse_line_args(self, args, line):
			self.size = hexval(args[0])
			return ''
		def __str__(self):
			return '0x{:08X}\tT (Size): 0x{:08X}'.format(self.offset, self.size)

	for t in [
		(0x01, 'CPDOWNSP', InstructionCPDOWNSP),
		(0x02, 'RSADD', InstructionRSADD),
		(0x03, 'CPTOPSP', InstructionCPTOPSP),
		(0x04, 'CONST', InstructionCONST),
		(0x05, 'ACTION', InstructionACTION),
		(0x06, 'LOGAND', InstructionLOGAND),
		(0x07, 'LOGOR', InstructionLOGOR),
		(0x08, 'INCOR', InstructionINCOR),
		(0x09, 'EXCOR', InstructionEXCOR),
		(0x0A, 'BOOLAND', InstructionBOOLAND),
		(0x0B, 'EQUAL', InstructionEQUAL),
		(0x0C, 'NEQUAL', InstructionNEQUAL),
		(0x0D, 'GEQ', InstructionGEQ),
		(0x0E, 'GT', InstructionGT),
		(0x0F, 'LT', InstructionLT),
		(0x10, 'LEQ', InstructionLEQ),
		(0x11, 'SHLEFT', InstructionSHLEFT),
		(0x12, 'SHRIGHT', InstructionSHRIGHT),
		(0x13, 'USHRIGHT', InstructionUSHRIGHT),
		(0x14, 'ADD', InstructionADD),
		(0x15, 'SUB', InstructionSUB),
		(0x16, 'MUL', InstructionMUL),
		(0x17, 'DIV', InstructionDIV),
		(0x18, 'MOD', InstructionMOD),
		(0x19, 'NEG', InstructionNEG),
		(0x1A, 'COMP', InstructionCOMP),
		(0x1B, 'MOVSP', InstructionMOVSP),
		(0x1C, 'STORE_STATEALL', InstructionSTORE_STATEALL),
		(0x1D, 'JMP', InstructionJMP),
		(0x1E, 'JSR', InstructionJSR),
		(0x1F, 'JZ', InstructionJZ),
		(0x20, 'RETN', InstructionRETN),
		(0x21, 'DESTRUCT', InstructionDESTRUCT),
		(0x22, 'NOT', InstructionNOT),
		(0x23, 'DECSP', InstructionDECSP),
		(0x24, 'INCSP', InstructionINCSP),
		(0x25, 'JNZ', InstructionJNZ),
		(0x26, 'CPDOWNBP', InstructionCPDOWNBP),
		(0x27, 'CPTOPBP', InstructionCPTOPBP),
		(0x28, 'DECBP', InstructionDECBP),
		(0x29, 'INCBP', InstructionINCBP),
		(0x2A, 'SAVEBP', InstructionSAVEBP),
		(0x2B, 'RESTOREBP', InstructionRESTOREBP),
		(0x2C, 'STORE_STATE', InstructionSTORE_STATE),
		(0x2D, 'NOP', InstructionNOP),
		(0x42, 'T', InstructionT),
	]:
		Instruction.add_type(*t)

	def extend(text_nwscript):
		ret = parser.parse(text_nwscript)
		InstructionACTION.functions = ret.functions
		for (type_engine, i) in ((ret.defines['ENGINE_STRUCTURE_' + str(i)].name, i) for i in range(0, ret.defines.get('ENGINE_NUM_STRUCTURES', 0))):
			a = arg_type.register(type_engine, type_engine, None)
			add_to_array(Instruction.arg_types, (0x10 + i), [a], None)
			if i < 10:
				add_to_array(Instruction.arg_types, (0x30 + i), [a, a], None)
	Instruction.extend = staticmethod(extend)
_extend_Instruction() # Add additional methods to Instruction class and register classes


