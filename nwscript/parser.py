import ply.yacc
import lexer

def parse(text):
	def p_nss_file(p):
		"""nss_file : empty
			         | statement_global nss_file"""
		pass

	def p_empty(p):
		"""empty :"""
		pass

	def p_statement_global(p):
		"""statement_global : statement_expression
			                 | function_def
			                 | include
			                 | define"""
		pass

	defines = {}
	def p_statement_define(p):
		"""define : DEFINE ID value_or_var"""
		# Register with global DEFINES
		defines[p[2].name] = p[3]

	def p_statement_include(p):
		"""include : INCLUDE STRING"""
		# TODO: Compile INCLUDE file as well
		pass

	class Function(object):
		def __init__(self, name, type_return, args, body = None):
			self.name = name
			self.type_return = type_return
			self.args = args
			self.body = body
	class FunctionParam(object):
		def __init__(self, t, name, valDefault = None):
			self.type = t
			self.name = name
			self.valueDefault = valDefault

	functions = []
	map_functions = {}
	def p_function_def(p):
		"""function_def : ID ID '(' params_def ')' function_body"""
		if p[6] == None: # Function marker, for full definition later
			f = Function(p[2].name, p[1].name, p[4])
			if map_functions.get(f.name, None):
				raise Exception('Already defined function: {}'.format(f.name))
			functions.append(f)
			map_functions[f.name] = f
		else:
			f = Function(p[2].name, p[1].name, p[4], p[6])
			fAlready = map_functions.get(f.name, None)
			if fAlready:
				if fAlready.body:
					raise Exception('Already defined function with body: {}'.format(f.name))
				fAlready.body = f.body
			else:
				functions.append(f)
				map_functions[f.name] = f
	def p_function_body(p):
		"""function_body : ';'
			              | '{' statement_list '}' """
		if p[1] == ';':
			p[0] = None

	def p_params_def(p):
		"""params_def : empty
			           | param_def
			           | param_def ',' params_def"""
		p[0] = ([p[1]] + p[3]) if (len(p) > 3) else ([p[1]] if p[1] else [])
	def p_param_def(p):
		"""param_def : ID ID
			          | ID ID '=' value_or_var"""
		p[0] = FunctionParam(p[2].name, p[1].name, p[4]) if (len(p) > 3) else FunctionParam(p[2].name, p[1].name)

	def p_statement_list(p):
		"""statement_list : statement
			               | statement statement_list"""
		pass

	def p_statement(p):
		"""statement : statement_expression
			          | statement_conditional"""
		pass
	def p_statement_expression(p):
		"""statement_expression : statement_assignment_target '=' expression ';'
			                     | expression ';'"""
		pass
	def p_statement_assignment_target(p):
		"""statement_assignment_target : ID
			                            | ID ID"""
		pass

	def p_expression(p):
		"""expression : expression_single
			           | expression_single link_expression expression
			           | '(' expression ')'"""
		pass
	def p_expression_single(p):
		"""expression_single : function_call
			                  | value_or_var"""
		pass
	def p_expression_list(p):
		"""expression_list : expression
			                | expression ',' expression_list"""
		pass
	def p_link_expression(p):
		"""link_expression : operator"""
		pass
	def p_operator(p):
		"""operator : OP_EQUALITY
			         | OP_COMPARISON
			         | '+'
			         | '-'
			         | '*'
			         | '/'"""
		pass

	def p_statement_conditional(p):
		"""statement_conditional : conditional statement
			                      | conditional '{' statement_list '}'"""
		pass
	def p_conditional(p):
		"""conditional : IF '(' expression ')'
			            | ELSE
			            | WHILE '(' expression ')'"""
		pass

	def p_function_call(p):
		"""function_call : ID '(' expression_list ')'"""
		pass

	def p_value_array(p):
		"""value_array : '[' value_array_list ']'"""
		pass
	def p_value_array_list(p):
		"""value_array_list : empty
			                 | expression
			                 | expression ',' value_array_list"""
		pass

	def p_value_or_val(p):
		"""value_or_var : value
			             | ID"""
		p[0] = p[1]
	def p_value(p):
		"""value : INTEGER
			      | FLOAT
			      | STRING
			      | value_array"""
		p[0] = p[1]
	def p_value_str(p):
		"""value_str : STRING"""
		pass

	def p_error(p):
		class ParserError(Exception):
			pass
		if p:
			raise ParserError('Parser error at line {}: {}'.format(p.lexer.lineno, p.value))
		else:
			raise ParserError('Unexpected End of File')

	nwlexer = lexer.lex()
	tokens = nwlexer.tokens
	parser = ply.yacc.yacc()
	parser.parse(text, lexer=nwlexer)
	class CodeData(object):
		def __init__(self):
			self.defines = defines
			self.functions = functions
			self.map_functions = map_functions
	return CodeData()

